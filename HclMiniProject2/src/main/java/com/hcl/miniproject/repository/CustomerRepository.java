package com.hcl.miniproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.miniproject.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
